import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './src/stateManagement/utils/store';
import {SafeAreaView} from 'react-native-safe-area-context';
import { StatusBar } from 'react-native';
import Routing from './src/stateManagement/Routing';

// export default function App() {
//   return (
//     <Provider store={store}>
//       <PersistGate loading={null} persistor={persistor}>
//         <SafeAreaView style={{flex: 1}}>
//           <StatusBar barstyle={'dark-content'} />
//           <Routing />
//         </SafeAreaView>
//       </PersistGate>
//     </Provider>
//   );
// }

const App = () => {
  return(
        <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex: 1}}>
          <StatusBar barstyle={'dark-content'} />
          <Routing/>
        </SafeAreaView>
      </PersistGate>
    </Provider>
  )
}

export default App;
