import { Text, View, TouchableOpacity} from 'react-native'
import React from 'react'
import {useSelector, useDispatch} from  'react-redux'

const Home = ({navigation}) => {

const dispatch = useDispatch()

const onLogout = async () =>{
    dispatch({type: "LOGOUT"})
    navigation.replace('Login')
}
const { isLoggedIn, userData } = useSelector((state) => state.auth);

console.log('cek userData', userData);

    return (
      <View>
        <Text>Home</Text>
        <TouchableOpacity onPress={onLogout}>
            <Text>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    )

}

export default Home